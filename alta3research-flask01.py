#!/usr/bin/python3
from flask import Flask, redirect, url_for
app = Flask(__name__)


@app.route("/anime/<names>")
def anime(names):    #to provide info on the anime given
    if names == "Pokemon":
        return { "Anime" : "Pokemon", 
                "Date" : "April 1, 1997",
                "Info" : "The anime franchise consists of seven sequential series in Japan , each based on a main installment of the Pokémon video game series."}
    elif names == "Deathnote":
        return { "Anime" : "Deathnote",
                "Date"  :"October 3, 2006 ",
                "Info" : "he series centers around Lights subsequent attempts to use the Death Note to carry out a worldwide massacre of individuals whom he deems immoral and to create a crime-free society"}
    else :
        return "Doesn't exist in the data collected"


@app.route("/rating/<inte>")
def hello_guest(inte):
    return f"Thank you for {inte} stars"



if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224) # runs the application

