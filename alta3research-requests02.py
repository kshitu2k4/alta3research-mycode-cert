#!/usr/bin/env python3

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests 
import requests

URL = "https://api.coindesk.com/v1/bpi/currentprice.json"   

def main():
    """sending GET request, checking response"""

    # API response is stored in "resp" object
    resp= requests.get(URL)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        output= resp.json()
        pprint(output)
        #only for retreiving currencies
        print('Currency value: ')
        pprint(output.get('bpi'))
    else:
        print("That is not a valid URL.")

if __name__ == "__main__":
    main()


